﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class ApproveTimeSheetController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage ApproveTimeSheet( int TimesheetID, int ApprovedUserID,  string Comment,string Status)
        {
            try
            {
                timesheet timesheet = null;
                using (var TSE = new minutesEntities())
                {
                      timesheet = TSE.timesheets.Where(u => u.timesheet_id == TimesheetID).FirstOrDefault<timesheet>(); ;

                    if (timesheet != null)
                    {
                        timesheet.status = Status;
                        timesheet.approved_user_id = ApprovedUserID;
                        timesheet.approved_date = DateTime.Now;
                        timesheet.approval_comment = Comment;
                    }
                    TSE.SaveChanges();
                    if (timesheet != null)
                    {
                        return Request.CreateResponse<timesheet>(HttpStatusCode.OK, timesheet);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, " Employee Not Found");
                    }  

                }
            }catch(Exception ex)
            {
                return null;
            }
        }
    }
}
