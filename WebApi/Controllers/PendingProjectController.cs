﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class PendingTimeSheetController : ApiController
    {
        public IQueryable getPendingProject(int LoogedUserID)
        {
            using (var context = new minutesEntities())
            {
                var result = from t in context.timesheets.Where(w => w.status == "Pending")
                             join p in context.projects on t.project_id equals p.project_id
                              select new
                             {
                                 t.timesheet_id,
                                 p.project_name,

                             };


                return result.ToList().Distinct().OrderBy(y => y.project_name).AsQueryable();
            }


        }
    }
}
