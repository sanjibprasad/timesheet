﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace New_Minutes.Models
{
    public class TimeSheet
    {
        public Int64 TimesheetId { get; set; }
        public string TimeWeekID { get; set; }
        public string USerID { get; set; }
        public string ProjectID { get; set; }
        public string Year { get; set; }
        public string Week { get; set; }
        public string Dated { get; set; }
        public string Status { get; set; }
        public string ApprovedByUserID { get; set; }
        public string ApprovedDate { get; set; }

        public List<OtherProjectData> OthePrjData = new List<OtherProjectData>(0);
        public class OtherProjectData
        {
            public string Dated = "";
            public Nullable<decimal> OverTime { get; set; }
            public Nullable<decimal> NormalTime { get; set; }
            public OtherProjectData(string Dated, Nullable<decimal> Ot, Nullable<decimal> Nt)
            {
                this.Dated = Dated;
                this.OverTime = Ot;
                this.NormalTime = Nt;
            }
        }
    }

    public class TimeSheetData
    {
        public Int64 TimesheetId { get; set; }
        public string TimeWeekID { get; set; }
        public int TaskID { get; set; }
        public string Date { get; set; }
        public Nullable<decimal> OverTime { get; set; }
        public Nullable<decimal> NormalTime { get; set; }
        public string Comment { get; set; }
        public string Ctrlid { get; set; }
    }

    public class TimeSheetColl : New_Minutes.Models.TimeSheet
    {
        public List<ProjectTasks> ProjectTask = new List<ProjectTasks>(0);
        public List<TimeSheetData> TimeData = new List<TimeSheetData>(0);
    }

    public class ProjectTasks
    {
        ProjectTasks() { }
        public int TaskId = 0;
        public string Description = "";

        public ProjectTasks(int TaskID, string Description)
        {
            this.TaskId = TaskID;
            this.Description = Description;
        }
    }

}