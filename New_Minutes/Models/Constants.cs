﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace New_Minutes.Models
{
    public static class Constants
    {
        public const string CPP = "CPP";
        public const string APR = "APR";
        public const string AEP = "AEP";
        public const string CTP = "CTP";
    }
}