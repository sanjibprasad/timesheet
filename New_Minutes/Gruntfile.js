﻿/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {
    grunt.initConfig({
        clean: ["Content/NewMinifiedUi/Thirdparty/js/*"],
        //concat: {
        //      all: {
        //        src: ['TypeScript/Tastes.js', 'TypeScript/Food.js'],
        //        dest: 'temp/combined.js'
        //      }
        //    },
    
        uglify: {
            //files: {
            //    src: 'Content/NewUi/Thirdparty/js/*.js',  // source files mask
            //    dest: 'Content/NewMinifiedUi/Thirdparty/js',    // destination folder
            //    expand: true,    // allow dynamic building
            //    flatten: true,   // remove all unnecessary nesting
            //    ext: '.min.js'   // replace .js to .min.js
            //}
            vendor: {
                src: 'Content/NewUi/js/vendor/*.js',  // source files mask
                dest: 'Min_Js/Content/NewUi/js/vendor/',    // destination folder
                expand: true,    // allow dynamic building
                flatten: true,   // remove all unnecessary nesting
                ext: '.min.js'   // replace .js to .min.js
            },
             js: {
                src: 'Content/NewUi/js/*.js',  // source files mask
                dest: 'Min_Js/Content/NewUi/js/',    // destination folder
                expand: true,    // allow dynamic building
                flatten: true,   // remove all unnecessary nesting
                ext: '.min.js'   // replace .js to .min.js
            },
            minjs: {
                files: grunt.file.expandMapping(['Scripts/Angular_Scripts/*.js', 'Scripts/Angular_Scripts/ActionItem/*.js', 'Scripts/Angular_Scripts/Global/*.js', 'Scripts/Angular_Scripts/Meeting/*.js', 'Scripts/Angular_Scripts/Notifications/*.js', 'Scripts/Angular_Scripts/Project/*.js', 'Scripts/Angular_Scripts/FactoryServices/*.js'], 'Min_Ang_Dev/', {
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.js', '.min.js');
                    }
                })
            },
            uimin: {
                files: grunt.file.expandMapping(['Content/NewUi/js/*.js', 'Content/NewUi/js/vendor/*.js'], 'Min_Js/', {
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.js', '.min.js');
                    }
                })

}
      

        },
        cssmin: {
            target: {
                minfiles: [{
                    expand: true,
                    cwd: 'Content/NewUi/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'Min_css/',
                    ext: '.min.css'
                }],
                files: grunt.file.expandMapping(['Content/NewUi/css/*.css', 'Content/NewUi/Thirdparty/css/*.css'], 'Min_Css/', {
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.css', '.min.css');
                    }
                })
            }
        },
        compress: {
            main: {
                options: {
                    mode: 'gzip'
                },
                files: grunt.file.expandMapping(['Min_Ang_Dev/Scripts/Angular_Scripts/*.js', 'Min_Ang_Dev/Scripts/Angular_Scripts/ActionItem/*.js', 'Min_Ang_Dev/Scripts/Angular_Scripts/Global/*.js', 'Min_Ang_Dev/Scripts/Angular_Scripts/Meeting/*.js', 'Min_Ang_Dev/Scripts/Angular_Scripts/Notifications/*.js', 'Min_Ang_Dev/Scripts/Angular_Scripts/Project/*.js', 'Min_Ang_Dev/Scripts/Angular_Scripts/FactoryServices/*.js'], 'GZ/', {
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.min.js', '.gz.js');
                    }
                })
            }
        },
        concat: {
              all: {
                  src: ['Min_Css/Content/NewUi/css/*.css'],
                dest: 'Combined/css/combined.min.css'
              }
            },

        watch: {
            js: { files: 'Content/NewMinifiedUi/Thirdparty/js/*.js', tasks: ['uglify','cssmin'] },
        }

    });
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.registerTask('default', ['uglify','cssmin']);

};
