﻿module.controller('TimeSheetCtrl', ['$scope', '$http', '$timeout', '$window', '$location', '$routeParams', '$localStorage', function ($scope, $http, $timeout, $window, $location, $routeParams, $localStorage) {

    var navUrl = $location.path();

    $scope.TimesheetData = {};

    $scope.TimesheetDate = {
        date: new Date(),
        datepickerOptions:
            {
                maxDate: null,
                showWeeks: false,
            }
    }

    $scope.openCalendar = function ($event, picker) {
        $scope[picker].open = true;
    }

    $scope.changedate = function () {
        var SelectedDate = $("#meeting-date").val();
        console.log(SelectedDate);
        TimeSheetDate = SelectedDate;
        $scope.LoadTimeSheet(TimeSheetDate);

    }

    /* employee id come from session but for time being please change the eid with employee_id in employees table in database */
    var eid = 12;
    var TimeSheetDate;
    var TimeSheetProject_id = 0;

    $http.get("http://localhost:52855/api/Project?Employeeid=" + eid).success(function (data) {
        console.log(data);
        $scope.Projects = data;

    });

    $scope.getResourceBasedOnProject = function (project_id, project_name) {

        TimeSheetProject_id = project_id;
        var SelectedDate = $("#meeting-date").val();
        TimeSheetDate = SelectedDate;
        $scope.LoadTimeSheet(TimeSheetDate);

    }
    $scope.LoadTimeSheet = function (TimeSheetDate) {


        var M = moment(TimeSheetDate, 'DD/MM/YYYY');
        SelDateWeek = M.week();
        var SelDateYear = M.year()

        var currentWekkVeek = moment().isoWeek();
        var currentWekkYear = moment().year();

        if (SelDateWeek > currentWekkVeek) {
            $('#idMSG').html('Time Sheet For Feature Week Is Not Allowed');
            popup_open('.Validate-Time');
        }
        else if (SelDateYear > currentWekkYear) {
            $('#idMSG').html('Time Sheet For Feature Year Is Not Allowed');
            popup_open('.Validate-Time');
        }
        else {

            $http.get("http://localhost:52855/api/TimeSheet?ProjectId=" + TimeSheetProject_id + "&LoogedUserID=" + eid + "&Year=" + SelDateYear + "&WeekID=" + SelDateWeek).success(function (data) {
                
                CreateTimeSheetGrid(TimeSheetDate, 'DD/MM/YYYY', 'DD/MM', 'ddd', data.ProjectTask, data.OthePrjData, data, SelDateWeek);
                $('#divTimeSheetbtn').css({ 'display': 'block' });
                if (data.Status == 'Pending' || data.Status == 'Approved') {
                    $('#divTimeSheetbtn').css({ 'display': 'none' });
                    $('#divMsg').html("Timesheet submitted, status is: " + data.Status);
                } else {
                    $('#divMsg').html("");
                }

            });
        }

    }

    /*  For Inserting Data using Angular js And Entity Framework  FYI*/
    $scope.SaveTimeSheetGridData = function () {

        jsonObj.Status = "InProgress";
        jsonObj.TimeWeekID = SelDateWeek;
        $scope.CloseTimeSheet();

    }
    $scope.SubmitTimeSheetGridData = function () {
        jsonObj.Status = "Pending";
        jsonObj.TimeWeekID = SelDateWeek;
        $scope.CloseTimeSheet();
    }

    $scope.CloseTimeSheet = function () {
        var obj = jsonObj;

        $http.post("http://localhost:52855/api/SaveTimeSheet", obj).success(function (data) {
            if (data == "0") {
                alert("Fail");
                // $scope.LoadTimeSheet(TimeSheetDate);
            } else {

                jsonObj.TimesheetId = data; //TimeSheetID
                popup_open('.save-popup');

            }
        })
    }




    /*  For Inserting Data using Angular js And Entity Framework  FYI*/
    $scope.saveDepartment = function () {

        var DepartmentDatas = {
            dept_name: $scope.DepartmentData.dept_name,
            record_status: "A",
            created_by: $localStorage.sessiondata.login_id

        }

        console.log(DesignationDatas);

        $http.post($localStorage.APIURL + 'LKPdata/InsertDepartment', DepartmentDatas).success(function (data) {
            if (data[0].result == 1) {
                console.log(data);
                $scope.deptId = data[1].dept_id;


                $scope.disable = false;
                $scope.CreateButton = false;
                $scope.employeeSuccess = true;
                $scope.update = false;

                $scope.buttonClicked = false;
                popup_open('.save-popup');

            }

            else {

                $scope.disable = true;
                $scope.CreateButton = true;
            }
            console.log(data[0].result);
            $scope.searchButtonText = "Save";
        })


    };





}
]);



