﻿module.controller('ApprovedTimeSheetCtrl', ['$scope', '$http', '$timeout', '$window', '$location', '$routeParams', '$localStorage', function ($scope, $http, $timeout, $window, $location, $routeParams, $localStorage) {

    var AppjsonObj = [];
    var AppselectedTextBox;
    var AppDbJsonObjColl;
    var AppSelDateWeek;
    var AppSelectedTimeSheetID;

    //var Approve_by = $localStorage.sessiondata.login_id;
    var Approve_by = 12;
    $scope.PendingTimeSheet = {};

    var navUrl = $location.path();

    $http.get("http://localhost:52855/api/PendingTimeSheet?LoogedUserID=" + Approve_by).success(function (data) {
        console.log(data);
        $scope.PendingProjects = data;
        if (data.length > 0) {
            $("#divApproveTimeSheetbtn").css({ 'display': 'block' });
        } else {
            $('#divApprovedMsg').html("NO ANY TIMESHEET AVAILABLE FOR APPROVE.");
            $("#divApproveTimeSheetbtn").css({ 'display': 'none' });
        }
    });

    $scope.getTimeSheetOnChange = function (TimeSheetid) {

        
        AppSelectedTimeSheetID = TimeSheetid.timesheet_id;
        $http.get("http://localhost:52855/api/TimeSheetForApprove?timesheetid=" + AppSelectedTimeSheetID + "&LoogedUserID=" + Approve_by).success(function (data) {

            $scope.CreateTimeSheetGridForApprove('DD/MM/YYYY', 'DD/MM', 'ddd', data.ProjectTask, data.OthePrjData, data, data.Week);
            $('#divTSApprovedGrid').css({ 'display': 'block' });
        });

    }

    $scope.ApproveTimeSheet = function () {
        var tsId = AppSelectedTimeSheetID;
        var comment = $("#TXTAREA_COMM").val().replace(/(['"])/g, "\\$1");
        var status = 'Approved';
        $scope.submitTimeSheet(tsId, Approve_by, comment, status);

    }

    $scope.RejectTimeSheet = function () {
        var tsId = AppSelectedTimeSheetID;
        var comment = $("#TXTAREA_COMM").val().replace(/(['"])/g, "\\$1");
        var status = 'InProgress';
        $scope.submitTimeSheet(tsId, Approve_by, comment, status);

    }

    $scope.submitTimeSheet = function (TSid, ApprovedUserID, Comment, status) {

        $http.post("http://localhost:52855/api/ApproveTimeSheet?TimesheetID=" + TSid + "&ApprovedUserID=" + ApprovedUserID + "&Comment=" + Comment + "&Status=" + status).success(function (data) {
            if (data == "0") {
                alert("Fail");
                console.log(data);
            } else {
                if (status == 'Approved') {
                    popup_open('.Approved-popup-ai');
                } else
                    popup_open('.Reject-popup-ai');

            }
        })
    }

    $scope.CreateTimeSheetGridForApprove = function (DateFormat, OutPutFormatDate, OutPutFormatDay, TaskColl, objectSavedTime, data, SelDateWeek) {

        AppSelDateWeek = SelDateWeek;
        //var date;
        //if (data.TimeData.length > 0) {
        //    var Dbdate = data.TimeData[0].Date;
        //    var arrDate = Dbdate.split('-');
        //    date = arrDate[2] + '/' + arrDate[1] + '/' + arrDate[0];
        //}
        var Dbdate = data.TimeData[0].Date
        AppSelectedTimeSheetID = data.TimesheetId;

        AppDbJsonObjColl = data;
        var objDiv = $('#divTSApprovedGrid').addClass('table-responsive');
        objDiv.html("");

        var tableHFixed = $scope.CreatePrevHourFixed(Dbdate, DateFormat, objectSavedTime);

        var table = $("<table />").attr('id', 'ApproveTimeSheetTbl').css({ 'width': '100%', 'margin-top': '0px', 'vertical-align': 'top' }).addClass('table table-bordered ');

        var WeekDaysDate = $scope.GetWeekDayAndDate(Dbdate, DateFormat, OutPutFormatDate, OutPutFormatDay);
        table.append(tableHFixed);
        var row = $("<tr/>").css({ 'background-color': '#f39c12' });
        row.append($('<th/>').text("#").attr('rowspan', '2').css({ 'vertical-align': 'bottom', 'width': '2%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).text("Work Item").attr('rowspan', '2').css({ 'vertical-align': 'bottom', 'width': '28%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[0]) + '</br>' + GetDay(WeekDaysDate[0])).attr('rowspan', '1').css({ 'width': '5%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[1]) + '</br>' + GetDay(WeekDaysDate[1])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[2]) + '</br>' + GetDay(WeekDaysDate[2])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[3]) + '</br>' + GetDay(WeekDaysDate[3])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[4]) + '</br>' + GetDay(WeekDaysDate[4])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[5]) + '</br>' + GetDay(WeekDaysDate[5])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[6]) + '</br>' + GetDay(WeekDaysDate[6])).attr('rowspan', '1').css({ 'width': '5%', 'background-color': '#f39c12' }));
        row.append($('<th/>').attr('colspan', 2).html("Total hours").attr('rowspan', '2').css({ 'vertical-align': 'bottom', 'width': '10%', 'background-color': '#f39c12' }));
        table.append(row);

        var rowW = $("<tr/>");
        rowW.append($('<th/>').attr('colspan', 2).text('OT').addClass('TSHeaderTop'));

        rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
        rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

        rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
        rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

        rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
        rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

        rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
        rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

        rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
        rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

        rowW.append($('<th/>').attr('colspan', 2).text('OT').addClass('TSHeaderTop'));

        table.append(rowW);

        for (var i = 0; i < TaskColl.length; i++) {
            var data = TaskColl[i];
            var TaskId = data.TaskId;
            var TaskName = data.Description;

            var dataRow = $scope.CreateTaskRow(i + 1, TaskId, TaskName, Dbdate, DateFormat);

            table.append(dataRow);
        }

        var descRow = $scope.SelectedItemDescriptionLabelRow();
        table.append(descRow);

        var commentRow = $scope.CreateCommentRow(i + 1);
        table.append(commentRow);

        //objDiv.append(tableHFixed);
        objDiv.append(table);

        $scope.SumOfAllTableRowInputTotal(TaskColl);

    }

    $scope.CreateTaskRow = function (index, TaskID, TaskName, date, DateFormat) {
        var rowW = $("<tr/>").attr('id', 'TR' + index);
        rowW.append($('<td/>').text(index).addClass('TSHeader').css({ 'border-top': '0px', 'border-bottom-width': '0px' }));

        rowW.append($('<td/>').attr('colspan', 2).text(TaskName).addClass('TSHeader').css({ 'border-top': '0px', 'border-bottom-width': '0px', 'text-align': 'left' }));

        var InputId = 'OT_SU_' + TaskID + '_' + index + "_" + MomentFullDate(0, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        var time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').attr('colspan', 2).append(input_OT).addClass('TSHeader'));

        InputId = 'NT_MO_' + TaskID + '_' + index + "_" + MomentFullDate(1, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_MO_' + TaskID + '_' + index + "_" + MomentFullDate(1, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'NT_TU_' + TaskID + '_' + index + "_" + MomentFullDate(2, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_TU_' + TaskID + '_' + index + "_" + MomentFullDate(2, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'NT_WE_' + TaskID + '_' + index + "_" + MomentFullDate(3, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_WE_' + TaskID + '_' + index + "_" + MomentFullDate(3, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'NT_TH_' + TaskID + '_' + index + "_" + MomentFullDate(4, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_TH_' + TaskID + '_' + index + "_" + MomentFullDate(4, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'NT_FR_' + TaskID + '_' + index + "_" + MomentFullDate(5, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_FR_' + TaskID + '_' + index + "_" + MomentFullDate(5, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'OT_SA_' + TaskID + '_' + index + "_" + MomentFullDate(6, date, DateFormat);
        $scope.CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { $scope.TextBoxFocus(this, event) });
        time = $scope.GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').attr('colspan', 2).append(input_OT).addClass('TSHeader'));

        // InputId = 'TOT_AL_' + TaskID + '_' + index + "_" + MomentFullDate(6, date, DateFormat);
        InputId = 'AppTOT_AL_' + index;
        var label_TOT = $('<label>').attr({ placeholder: 'OT', id: InputId });
        rowW.append($('<td/>').attr('colspan', 2).append(label_TOT).addClass('TSHeader'));

        return rowW;
    }

    $scope.SumOfAllTableRowInputTotal = function (TaskColl) {
        for (var index = 1; index < TaskColl.length + 1; index++) {
            //Sum of week days hours
            var tr = $('#TR' + index + ' input');
            var TotalSum = 0;
            tr.each(function () {
                var _value = $(this).val();
                if (!isNaN(_value) && _value.length != 0) {
                    TotalSum += parseFloat(_value);
                }
            });

            var tbl = $('#ApproveTimeSheetTbl');
            var label_TOT = $(tbl).find($("#AppTOT_AL_" + index));
            label_TOT.html(TotalSum);
        }
    }

    $scope.SelectedItemDescriptionLabelRow = function () {
        var rowComm = $("<tr/>");

        var label = $("<label>").attr('for', "lblDescription");
        rowComm.append($('<td/>').attr('colspan', 6).append(label).css({ 'border-top': '0px', 'border-bottom-width': '0px', 'text-align': 'left', 'valign': 'middle' }));
        var labelComments = $("<label>").attr('for', "lblComments");
        rowComm.append($('<td/>').attr('colspan', 12).append(labelComments).css({ 'border-top': '0px', 'border-bottom-width': '0px', 'text-align': 'left', 'valign': 'middle' }));

        return rowComm;
    }

    $scope.CreateCommentRow = function (index) {

        var rowComm = $("<tr/>").attr('data-id', index);
        rowComm.append($('<td/>').attr('colspan', 3).text("Approval Comments").addClass('TSHeader').css({ 'border-top': '0px', 'border-bottom-width': '0px', 'text-align': 'center', 'valign': 'middle' }));

        var input_OT = $('<textarea>').attr({ type: 'text', placeholder: ' Approval Comments', id: 'TXTAREA_COMM' }).css({ 'width':'100%' });;

        rowComm.append($('<td/>').attr('colspan', 15).append(input_OT).addClass('TSHeader'));
        return rowComm;
    }

    $scope.CreatePrevHourFixed = function (date, DateFormat, objectSavedTime) {

        var Daydate = $scope.MomentFullDate(0, date, DateFormat);

        var tableHFixed = $("<table />").attr('id', 'TimeSheetTblHfixed').css({ 'width': '100%', 'margin-top': '5px', 'margin-bottom': '0px' }).addClass('table  prev-hours-fixed');

        var rowCTime = $("<tr/>");
        // rowCTime.append($('<th/>').html("").css({ 'vertical-align': 'bottom', 'width': '10%' }));
        rowCTime.append($('<th/>').attr('colspan', 4).text("Hours already posted in other projects for the selected week").css({ 'vertical-align': 'bottom', 'width': '25.5%' }).addClass('border-gray'));

        var prevTime = $scope.GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_OT_SU').html(prevTime).css({ 'width': '5%', 'colspan': '2' }).addClass('border-gray'));

        Daydate = $scope.MomentFullDate(1, date, DateFormat);
        prevTime = $scope.GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_NT_MO').html(prevTime).css({ 'width': '5%' }));

        prevTime = $scope.GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_OT_MO').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

        Daydate = $scope.MomentFullDate(2, date, DateFormat);
        prevTime = $scope.GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_NT_TU').html(prevTime).css({ 'width': '5%' }));

        prevTime = GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_OT_TU').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

        Daydate = $scope.MomentFullDate(3, date, DateFormat);
        prevTime = $scope.GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_NT_WE').html(prevTime).css({ 'width': '5%' }));

        prevTime = $scope.GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_OT_WE').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

        Daydate = $scope.MomentFullDate(4, date, DateFormat);
        prevTime = $scope.GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_NT_TH').html(prevTime).css({ 'width': '5%' }));

        prevTime = $scope.GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_OT_TH').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

        Daydate = $scope.MomentFullDate(5, date, DateFormat);
        prevTime = $scope.GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_NT_FR').html(prevTime).css({ 'width': '5%' }));

        prevTime = $scope.GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_OT_FR').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

        Daydate = $scope.MomentFullDate(6, date, DateFormat);
        prevTime = $scope.GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
        rowCTime.append($('<th/>').attr('id', 'TH_OT_ST').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

        rowCTime.append($('<th/>').attr('colspan', 3).html("").css({ 'vertical-align': 'bottom', 'width': '10%' }));
        // rowCTime.append($('<th/>').html("").css({ 'vertical-align': 'bottom', 'width': '10%' }));

        //tableHFixed.append(rowCTime);

        return rowCTime;
    }

    $scope.GetPrevHourFixed = function (Date, Time, objectSavedTime) {

        var Cdate = Date.split('_').join('-');
        var prevtime = 0;
        $.each(objectSavedTime, function (k, v) {
            if (v.Dated == Cdate) {

                if (Time == 'OT') {
                    prevtime = v.OverTime;
                } else {
                    prevtime = v.NormalTime;
                }
                return prevtime;
            }
        });
        return prevtime;
    }

    $scope.GetSavedValue = function (ctrlId) {

        var time = '';
        if (AppDbJsonObjColl != null) {
            var objTimeData = AppDbJsonObjColl.TimeData;
            if (objTimeData.length > 0) {
                $.each(objTimeData, function (k, v) {
                    if (v.Ctrlid == ctrlId) {

                        if (ctrlId.contains('OT')) {
                            if (v.OverTime != null)
                                time = v.OverTime;
                        } else {
                            if (v.NormalTime != null)
                                time = v.NormalTime;
                        }
                        $scope.SetSaveComment(ctrlId, v.Comment)
                        return time;
                    };
                });
                return time;
            }
        }
    }

    $scope.SetSaveComment = function (ctrlId, comment) {
        $.each(AppjsonObj, function (k, v) {
            if (v.ctrlID == ctrlId) {
                v.Comment = comment;
                return;
            }
        });
    }

    $scope.TextBoxFocus = function (object, event) {
        $('label[for="lblComments"]').text('');
        AppselectedTextBox = object;
        $scope.EnteryDetails(object);

        $.each(AppjsonObj, function (k, v) {
            if (v.ctrlID == object.id) {
                if (v.Comment != '') {
                    $('label[for="lblComments"]').text(v.Comment).css('color', 'blue');
                }
                return;
            }
        });

    }

    $scope.EnteryDetails = function (objSelectedTextbox) {
        try {
            var arrName = objSelectedTextbox.id.split('_')
            var index = arrName[3];

            var tr = $('#TR' + index);
            var col1 = tr.find("td:eq(1)").text();

            var descText = "Entry Details: " + col1 + " - " + arrName[4] + "/" + arrName[5] + "/" + arrName[6];

            $('label[for="lblDescription"]').text(descText).css('color', 'blue');

        } catch (e) {
            alert(e.description);
        }
    }

    $scope.GetDay = function (daysAndDate) {
        var Day;
        if (daysAndDate.length > 0) {
            var DD = daysAndDate.split('~');
            Day = DD[0];
        }

        return Day;
    }

    $scope.GetDate = function (daysAndDate) {
        var Date;
        if (daysAndDate.length > 0) {
            var DD = daysAndDate.split('~');
            Date = DD[1];
        }
        return Date;
    }

    $scope.GetWeekDayAndDate = function (date, DateFormat, OutPutFormatDate, OutPutFormatDay) {
        var WeekDaysAndDate = [];

        var M = moment(date, DateFormat);
        WeekDaysAndDate[0] = $scope.MomentWeekDayAndDate(0, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
        WeekDaysAndDate[1] = $scope.MomentWeekDayAndDate(1, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
        WeekDaysAndDate[2] = $scope.MomentWeekDayAndDate(2, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
        WeekDaysAndDate[3] = $scope.MomentWeekDayAndDate(3, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
        WeekDaysAndDate[4] = $scope.MomentWeekDayAndDate(4, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
        WeekDaysAndDate[5] = $scope.MomentWeekDayAndDate(5, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
        WeekDaysAndDate[6] = $scope.MomentWeekDayAndDate(6, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
        return WeekDaysAndDate;
    }

    $scope.MomentWeekDayAndDate = function (Day, date, DateFormat, OutPutFormatDate, OutPutFormatDay) {
        var ddDate;
        var M = moment(date, DateFormat);
        ddDate = M.subtract('weeks', 0).day(Day).format(OutPutFormatDay) + "~" + M.subtract('weeks', 0).day(Day).format(OutPutFormatDate);
        return ddDate;
    }

    $scope.MomentFullDate = function (Day, date, DateFormat) {
        var ddDate;
        var M = moment(date, DateFormat);
        ddDate = M.subtract('weeks', 0).day(Day).format('DD/MM/YYYY');
        return ddDate.split('/').join('_');
    }

    $scope.CreateJSONObjectClass = function (ctrlID) {
        if (ctrlID.length > 0) {
            var data = ctrlID.split('_');

            var item = {
                'TimesheetId': "",
                'TimeWeekID': AppSelDateWeek,
                'TaskID': data[2],
                'Date': data[5] + '/' + data[4] + '/' + data[6], //MM/DD/YYYY
                'OverTime': 0,
                'NormalTime': 0,
                'Comment': "",
                'ctrlID': ctrlID
            }
            AppjsonObj.push(item);
        }

    }
}
]);