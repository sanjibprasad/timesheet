﻿var jsonObj = [];
var selectedTextBox;
var SelDateWeek;

function CreateTimeSheetGrid(date, DateFormat, OutPutFormatDate, OutPutFormatDay, TaskColl, objectSavedTime, data, SelDateWeek) {
    SelDateWeek = SelDateWeek;
    jsonObj = [];
    jsonObj = data;

    var objDiv = $('#divTSConainerGrid').addClass('table-responsive');
    objDiv.html("");

    var tableHFixed = CreatePrevHourFixed(date, DateFormat, objectSavedTime);

    var table = $("<table />").attr('id', 'TimeSheetTbl').css({ 'width': '100%', 'margin-top': '0px', 'vertical-align': 'top' }).addClass('table table-bordered ');

    var WeekDaysDate = GetWeekDayAndDate(date, DateFormat, OutPutFormatDate, OutPutFormatDay);
    table.append(tableHFixed);
    var row = $("<tr/>").css({ 'background-color': '#f39c12' });
    row.append($('<th/>').text("#").attr('rowspan', '2').css({ 'vertical-align': 'bottom', 'width': '2%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).text("Work Item").attr('rowspan', '2').css({ 'vertical-align': 'bottom', 'width': '28%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[0]) + '</br>' + GetDay(WeekDaysDate[0])).attr('rowspan', '1').css({ 'width': '5%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[1]) + '</br>' + GetDay(WeekDaysDate[1])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[2]) + '</br>' + GetDay(WeekDaysDate[2])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[3]) + '</br>' + GetDay(WeekDaysDate[3])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[4]) + '</br>' + GetDay(WeekDaysDate[4])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[5]) + '</br>' + GetDay(WeekDaysDate[5])).attr('rowspan', '1').css({ 'width': '10%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).html(GetDate(WeekDaysDate[6]) + '</br>' + GetDay(WeekDaysDate[6])).attr('rowspan', '1').css({ 'width': '5%', 'background-color': '#f39c12' }));
    row.append($('<th/>').attr('colspan', 2).html("Total hours").attr('rowspan', '2').css({ 'vertical-align': 'bottom', 'width': '10%', 'background-color': '#f39c12' }));
    table.append(row);


    var rowW = $("<tr/>");
    rowW.append($('<th/>').attr('colspan', 2).text('OT').addClass('TSHeaderTop'));

    rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
    rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

    rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
    rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

    rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
    rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

    rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
    rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

    rowW.append($('<th/>').text('NT').addClass('TSHeaderTop'));
    rowW.append($('<th/>').text('OT').addClass('TSHeaderTop'));

    rowW.append($('<th/>').attr('colspan', 2).text('OT').addClass('TSHeaderTop'));

    table.append(rowW);

    for (var i = 0; i < TaskColl.length; i++) {
        var data = TaskColl[i];
        var TaskId = data.TaskId;
        var TaskName = data.Description;

        var dataRow = CreateTaskRow(i + 1, TaskId, TaskName, date, DateFormat);

        table.append(dataRow);
    }

    var descRow = SelectedItemDescriptionLabelRow();
    table.append(descRow);

    var commentRow = CreateCommentRow(i + 1);
    table.append(commentRow);

    //objDiv.append(tableHFixed);
    objDiv.append(table);

    SumOfAllTableRowInputTotal(TaskColl);
}

function SumOfAllTableRowInputTotal(TaskColl) {
    try {
        for (var index = 1; index < TaskColl.length + 1; index++) {
            //Sum of week days hours
            var tr = $('#TR' + index + ' input');
            var TotalSum = 0;
            tr.each(function () {
                var _value = $(this).val();
                if (!isNaN(_value) && _value.length != 0) {
                    TotalSum += parseFloat(_value);
                }
            });

            var tbl = $('#TimeSheetTbl');
            var label_TOT = $(tbl).find($("#TOT_AL_" + index));
            label_TOT.html(TotalSum);
        }
    } catch (e) {
        alert(e.description);
    }
}

function SelectedItemDescriptionLabelRow() {
    var rowComm = $("<tr/>");

    var label = $("<label>").attr('for', "lblDescription");
    rowComm.append($('<td/>').attr('colspan', 18).append(label).css({ 'border-top': '0px', 'border-bottom-width': '0px', 'text-align': 'left', 'valign': 'middle' }));

    return rowComm;
}

function CreateCommentRow(index) {

    var rowComm = $("<tr/>").attr('data-id', index);
    rowComm.append($('<td/>').attr('colspan', 3).text("Comments").addClass('TSHeader').css({ 'border-top': '0px', 'border-bottom-width': '0px', 'text-align': 'center', 'valign': 'middle' }));

    var input_OT = $('<textarea>').focus(function () { CommentFocus(this) }).blur(function () { CommentBlur(this) }).attr({ type: 'text', placeholder: 'Comments', id: 'TXTAREA_COMM' });

    rowComm.append($('<td/>').attr('colspan', 14).append(input_OT).addClass('TSHeader'));

    rowComm.append($('<td/>').attr('colspan', 1));

    return rowComm;
}

function CommentBlur(object) {

    if (selectedTextBox != null) {
        $.each(jsonObj.TimeData, function (k, v) {
            if (v.ctrlID == selectedTextBox.id) {
                v.Comment = object.value;
                return;
            }
        });
        $('#' + selectedTextBox.id).css("border", "");
    }

}

function CommentFocus(obj) {
    try {
        if (selectedTextBox != null) {
            $('#' + selectedTextBox.id).css("border", "2px solid red");
        }
    } catch (e) {
        alert(e.description);
    }
}

function CreatePrevHourFixed(date, DateFormat, objectSavedTime) {

    var Daydate = MomentFullDate(0, date, DateFormat);

    var tableHFixed = $("<table />").attr('id', 'TimeSheetTblHfixed').css({ 'width': '100%', 'margin-top': '5px', 'margin-bottom': '0px' }).addClass('table  prev-hours-fixed');

    var rowCTime = $("<tr/>");
    // rowCTime.append($('<th/>').html("").css({ 'vertical-align': 'bottom', 'width': '10%' }));
    rowCTime.append($('<th/>').attr('colspan', 4).text("Hours already posted in other projects for the selected week").css({ 'vertical-align': 'bottom', 'width': '25.5%' }).addClass('border-gray'));

    var prevTime = GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_OT_SU').html(prevTime).css({ 'width': '5%', 'colspan': '2' }).addClass('border-gray'));

    Daydate = MomentFullDate(1, date, DateFormat);
    prevTime = GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_NT_MO').html(prevTime).css({ 'width': '5%' }));

    prevTime = GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_OT_MO').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

    Daydate = MomentFullDate(2, date, DateFormat);
    prevTime = GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_NT_TU').html(prevTime).css({ 'width': '5%' }));

    prevTime = GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_OT_TU').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

    Daydate = MomentFullDate(3, date, DateFormat);
    prevTime = GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_NT_WE').html(prevTime).css({ 'width': '5%' }));

    prevTime = GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_OT_WE').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

    Daydate = MomentFullDate(4, date, DateFormat);
    prevTime = GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_NT_TH').html(prevTime).css({ 'width': '5%' }));

    prevTime = GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_OT_TH').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

    
    Daydate = MomentFullDate(5, date, DateFormat);
    prevTime = GetPrevHourFixed(Daydate, 'NT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_NT_FR').html(prevTime).css({ 'width': '5%' }));

    prevTime = GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_OT_FR').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

    Daydate = MomentFullDate(6, date, DateFormat);
    prevTime = GetPrevHourFixed(Daydate, 'OT', objectSavedTime);
    rowCTime.append($('<th/>').attr('id', 'TH_OT_ST').html(prevTime).css({ 'width': '5%' }).addClass('border-gray'));

    rowCTime.append($('<th/>').attr('colspan', 3).html("").css({ 'vertical-align': 'bottom', 'width': '10%' }));
    // rowCTime.append($('<th/>').html("").css({ 'vertical-align': 'bottom', 'width': '10%' }));

    //tableHFixed.append(rowCTime);

    return rowCTime;
}

function GetPrevHourFixed(Date, Time, objectSavedTime) {

    var Cdate = Date.split('_').join('-');
    var prevtime = 0;
    $.each(objectSavedTime, function (k, v) {
        if (v.Dated == Cdate) {

            if (Time == 'OT') {
                prevtime = v.OverTime;
            } else {
                prevtime = v.NormalTime;
            }
            return prevtime;
        }
    });
    return prevtime;
}

function CreateTaskRow(index, TaskID, TaskName, date, DateFormat) {
    try {
        
        var rowW = $("<tr/>").attr('id', 'TR' + index);
        rowW.append($('<td/>').text(index).addClass('TSHeader').css({ 'border-top': '0px', 'border-bottom-width': '0px' }));

        rowW.append($('<td/>').attr('colspan', 2).text(TaskName).addClass('TSHeader').css({ 'border-top': '0px', 'border-bottom-width': '0px', 'text-align': 'left' }));

        var InputId = 'OT_SU_' + TaskID + '_' + index + "_" + MomentFullDate(0, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        var time = GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').attr('colspan', 2).append(input_OT).addClass('TSHeader'));

        InputId = 'NT_MO_' + TaskID + '_' + index + "_" + MomentFullDate(1, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_MO_' + TaskID + '_' + index + "_" + MomentFullDate(1, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'NT_TU_' + TaskID + '_' + index + "_" + MomentFullDate(2, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_TU_' + TaskID + '_' + index + "_" + MomentFullDate(2, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'NT_WE_' + TaskID + '_' + index + "_" + MomentFullDate(3, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_WE_' + TaskID + '_' + index + "_" + MomentFullDate(3, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'NT_TH_' + TaskID + '_' + index + "_" + MomentFullDate(4, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_TH_' + TaskID + '_' + index + "_" + MomentFullDate(4, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'NT_FR_' + TaskID + '_' + index + "_" + MomentFullDate(5, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_NT = $('<input>').attr({ type: 'text', placeholder: 'NT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_NT.val(time);
        rowW.append($('<td/>').append(input_NT).addClass('TSHeader'));

        InputId = 'OT_FR_' + TaskID + '_' + index + "_" + MomentFullDate(5, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').append(input_OT).addClass('TSHeader'));

        InputId = 'OT_SA_' + TaskID + '_' + index + "_" + MomentFullDate(6, date, DateFormat);
        CreateJSONObjectClass(InputId)
        var input_OT = $('<input>').attr({ type: 'text', placeholder: 'OT', size: '2', id: InputId }).focus(function () { TextBoxFocus(this, event) }).blur(function () { TextBoxBlur(this, event) });
        time = GetSavedValue(InputId);
        if (time != '')
            input_OT.val(time);
        rowW.append($('<td/>').attr('colspan', 2).append(input_OT).addClass('TSHeader'));

        // InputId = 'TOT_AL_' + TaskID + '_' + index + "_" + MomentFullDate(6, date, DateFormat);
        InputId = 'TOT_AL_' + index;
        var label_TOT = $('<label>').attr({ placeholder: 'OT', id: InputId });
        rowW.append($('<td/>').attr('colspan', 2).append(label_TOT).addClass('TSHeader'));

        return rowW;
    } catch (e) {
        alert(e.description);
    }
}

function GetSavedValue(ctrlId) {

    var time = '';
    var objTimeData = jsonObj.TimeData;
    if (objTimeData.length > 0) {
        $.each(objTimeData, function (k, v) {
            
            if (v.Ctrlid == ctrlId) {

                if (ctrlId.contains('OT')) {
                    if (v.OverTime != null)
                        time = v.OverTime;
                } else {
                    if (v.NormalTime != null)
                        time = v.NormalTime;
                }
                SetSaveComment(ctrlId, v.Comment)
                return time;
            };
        });
        return time;
    }

}

function SetSaveComment(ctrlId, comment) {
    $.each(jsonObj.TimeData, function (k, v) {
        if (v.ctrlID == ctrlId) {
            v.Comment = comment;
            return;
        }
    });
}

function TextBoxBlur(object, e) {

    SetZeroValue(object);
    var inputVal = isNaN(parseFloat((object).value)) ? 0 : parseFloat((object).value);
    if (inputVal > 24) {
        //alert('Please enter value less then 24.')
        $('#idMSG').html('Input time has been rejected.<br />Please put time Numeric, less then 24 hours, Negative time not allowed');
        popup_open('.Validate-Time');
        object.value = "";
    }

   

    var inputValue = object.value
    if (inputValue != "") {
        var validValue = isValidTime(inputValue);
        if (validValue == true) {

            if (isTimeGreaterThenAllValue(object)) {

                if (object.id.length > 0) {
                    var arrName = object.id.split('_');
                    var index = arrName[3];
                    var TaskID = arrName[2];

                    //Sum of week days hours
                    var tr = $('#TR' + index + ' input');
                    var TotalSum = 0;
                    tr.each(function () {
                        var _value = $(this).val();
                        if (!isNaN(_value) && _value.length != 0) {
                            TotalSum += parseFloat(_value);
                        }
                    });

                    var tbl = $('#TimeSheetTbl');
                    var label_TOT = $(tbl).find($("#TOT_AL_" + index));
                    label_TOT.html(TotalSum);
                    //end sum

                    $.each(jsonObj.TimeData, function (k, v) {
                        if (v.ctrlID == object.id) {
                            if (object.id.contains('OT')) {
                                v.OverTime = object.value;
                            } else {
                                v.NormalTime = object.value;
                            }
                            return;
                        }
                    });

                }
            } else {
                object.value = "";
                popup_open('.Validate-Time');
            }
        } else {
            object.value = "";
            popup_open('.Validate-Time');
        }
    }else
    {
        SetZeroValue(object);
    }


}

function SetZeroValue(object)
{
    $.each(jsonObj.TimeData, function (k, v) {
        if (v.ctrlID == object.id) {
            if (object.id.contains('OT')) {
                debugger;
                v.OverTime = 0;
            } else {
                v.NormalTime = 0;
            }
            return;
        }
    });
}

function isValidTime(strVal) {
    if (isNaN(strVal)) return false;
    if (parseInt(strVal) < 0) return false;
    //try{
    //    if (strVal.contains('.')) {
    //        if (strVal.indexOf('.') > -1) return true;
    //    }
    //}catch(e){}
    return true;
}

function isTimeGreaterThenAllValue(objectInput) {
    var inputValue = objectInput.value
    if (objectInput.id.length > 0) {
        var arrId = objectInput.id.split('_');
        var index = arrId[3];
        var TaskID = arrId[2];
        var Date = arrId[4] + '-' + arrId[5] + '-' + arrId[6];
        var TotalSum = 0;

        var TimeShhetData = jsonObj.TimeData;
        var prevTimeData = jsonObj.OthePrjData;

        $.each(prevTimeData, function (k, v) {
            if (v.Dated == Date) {
                if (isValidTime(v.OverTime))
                    TotalSum += parseFloat(v.OverTime);

                if (isValidTime(v.NormalTime))
                    TotalSum += parseFloat(v.NormalTime);
            }
        });


        var Date2 = arrId[5] + '/' + arrId[4] + '/' + arrId[6];
        $.each(TimeShhetData, function (k, v) {
            if (v.Date == Date2) {

                if (isValidTime(v.OverTime))
                    TotalSum += parseFloat(v.OverTime);

                if (isValidTime(v.NormalTime))
                    TotalSum += parseFloat(v.NormalTime);
            }
        });

    }

    if (isValidTime(inputValue))
        TotalSum += parseFloat(inputValue);

    if (TotalSum > 24) {
        return false;
    } else { return true; }

}


function TextBoxFocus(object, event) {

    $.each(jsonObj.TimeData, function (k, v) {
        if (v.ctrlID == object.id) {
            if (object.id.contains('OT')) {
                v.OverTime = 0;
            } else {
                v.NormalTime = 0;
            }

        }
    });

    $('#TXTAREA_COMM').val('');
    //Change object border and color
    selectedTextBox = object;
    EnteryDetails(object);

    $.each(jsonObj.TimeData, function (k, v) {
        if (v.ctrlID == object.id) {
            if (v.Comment != '') {
                $('#TXTAREA_COMM').val(v.Comment);
            }
            return;
        }
    });


}


function EnteryDetails(objSelectedTextbox) {
    try {
        var arrName = objSelectedTextbox.id.split('_')
        var index = arrName[3];

        var tr = $('#TR' + index);
        var col1 = tr.find("td:eq(1)").text();

        var descText = "Entry Details: " + col1 + " - " + arrName[4] + "/" + arrName[5] + "/" + arrName[6];

        $('label[for="lblDescription"]').text(descText).css('color', 'blue');

    } catch (e) {
        alert(e.description);
    }
}

function GetDay(daysAndDate) {
    var Day;
    if (daysAndDate.length > 0) {
        var DD = daysAndDate.split('~');
        Day = DD[0];
    }

    return Day;
}

function GetDate(daysAndDate) {
    var Date;
    if (daysAndDate.length > 0) {
        var DD = daysAndDate.split('~');
        Date = DD[1];
    }
    return Date;
}

function GetWeekDayAndDate(date, DateFormat, OutPutFormatDate, OutPutFormatDay) {
    var WeekDaysAndDate = [];

    var M = moment(date, DateFormat);
    WeekDaysAndDate[0] = MomentWeekDayAndDate(0, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
    WeekDaysAndDate[1] = MomentWeekDayAndDate(1, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
    WeekDaysAndDate[2] = MomentWeekDayAndDate(2, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
    WeekDaysAndDate[3] = MomentWeekDayAndDate(3, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
    WeekDaysAndDate[4] = MomentWeekDayAndDate(4, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
    WeekDaysAndDate[5] = MomentWeekDayAndDate(5, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
    WeekDaysAndDate[6] = MomentWeekDayAndDate(6, date, DateFormat, OutPutFormatDate, OutPutFormatDay)
    return WeekDaysAndDate;
}

function MomentWeekDayAndDate(Day, date, DateFormat, OutPutFormatDate, OutPutFormatDay) {
    var ddDate;
    var M = moment(date, DateFormat);
    ddDate = M.subtract('weeks', 0).day(Day).format(OutPutFormatDay) + "~" + M.subtract('weeks', 0).day(Day).format(OutPutFormatDate);
    return ddDate;
}

function MomentFullDate(Day, date, DateFormat) {
    var ddDate;
    var M = moment(date, DateFormat);
    ddDate = M.subtract('weeks', 0).day(Day).format('DD/MM/YYYY');
    return ddDate.split('/').join('_');
}

function CreateJSONObjectClass(ctrlID) {
    var isIDExist = false;
    $.each(jsonObj.TimeData, function (k, v) {
        if (v.ctrlid == ctrlID) {
            isIDExist = true;
        }
    });

    if (isIDExist == false) {
        var data = ctrlID.split('_');
        var item = {
            'TimesheetId': "",
            'TimeWeekID': SelDateWeek,
            'TaskID': data[2],
            'Date': data[5] + '/' + data[4] + '/' + data[6], //MM/DD/YYYY
            'OverTime': 0,
            'NormalTime': 0,
            'Comment': "",
            'ctrlID': ctrlID
        }
        jsonObj.TimeData.push(item);
    }


}